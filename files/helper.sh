#!/bin/bash

get_runner_section() {
  search_str=$1
  runner_sections=($(grep -n '\[\[runners\]\]' /etc/gitlab-runner/config.toml | cut -f1 -d:))
  for (( sect_num=0; sect_num<${#runner_sections[@]}; sect_num++ )); do
    if [[ "$sect_num" == $(( ${#runner_sections[@]} -1 )) ]]; then
      next='$'
    else
      next=$((${runner_sections[sect_num + 1]} -1 ))
    fi
    section_range="${runner_sections[sect_num]},$next"
    section=$(sed "$section_range!d" /etc/gitlab-runner/config.toml)
    if echo "$section" | grep -q "$search_str"; then
      echo -e "$section" | sed 's/"//g'
      return 0
    fi
  done
}

get_token() {
  name=$1
  get_runner_section "name = \"$name\"" | awk '/token/ { print $NF }'
}

get_runnerid() {
  name=$1
  get_runner_section "name = \"$name\"" | awk '/runner_id/ { print $NF }'
}


main() {
  case "$1" in
    "get_token")
      get_token "$2"
      ;;
    "get_runnerid")
      get_runnerid "$2"
      ;;
    *)
      echo "Invalid command"
      exit 1
      ;;
  esac
}

main "$@"
