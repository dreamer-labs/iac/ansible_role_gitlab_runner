import os
import json
import time

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("gitlab_server")


def test_runner_job_success(host):
    access_token = "token-string-here234"
    project_url = "https://gitlab.test.int/api/v4/projects/2"

    pipeline = json.loads(
        host.run(
            f"curl -s -X POST --header 'PRIVATE-TOKEN: {access_token}' "
            f"{project_url}/pipeline?ref=master"
        ).stdout
    )

    result = "pending"
    while result == "pending" or result == "running" or result == "created":

        pipeline_result = json.loads(
            host.run(
                f"curl -s -X GET --header 'PRIVATE-TOKEN: {access_token}' "
                f"{project_url}/pipelines/{pipeline['id']}"
            ).stdout
        )
        result = pipeline_result["status"]
        time.sleep(5)

    if pipeline_result["status"] == "failed":
        job_results = []
        # Get list of jobs
        job_list = json.loads(
            host.run(
                f"curl -s -X GET --header 'PRIVATE-TOKEN: {access_token}' "
                f"{project_url}/pipelines/{pipeline['id']}/jobs"
            ).stdout
        )

        for job in job_list:
            job_result = (
                host.run(
                    f"curl -s -X GET --header 'PRIVATE-TOKEN: {access_token}' "
                    f"{project_url}/jobs/{job['id']}/trace"
                ).stdout
            )
            job_results.append(job_result)

    assert (
        pipeline_result["status"] == "success"
    ), f"Pipeline results: {pipeline_result}\nJob results: {job_result}"
