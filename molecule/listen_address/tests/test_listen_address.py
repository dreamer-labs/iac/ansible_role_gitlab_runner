import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_metrics_accessible(host):

    status = host.run('curl http://localhost:4443/metrics')

    assert status.rc == 0


def test_runner_registered(host):
    runner_status = host.run("gitlab-runner verify")

    assert runner_status.rc == 0
