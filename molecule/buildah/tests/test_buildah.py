import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_buildah(host):
    command = ("docker run quay.io/containers/buildah:latest "
               "/bin/bash -c 'export STORAGE_DRIVER=vfs && buildah info'")
    with host.sudo():
        buildah_status = host.run(command)

    assert buildah_status.rc == 0
