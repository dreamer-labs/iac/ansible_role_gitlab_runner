import json
import os
import pytest
import urllib3

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture
def gitlab_runner_id(host):
    runner_id = host.run(
        "grep runner_id /etc/gitlab-runner/config.toml | awk '{ print $NF }'"
    )

    return runner_id.stdout.rstrip()


def test_runner_has_new_tags(host, ansible_vars, gitlab_runner_id):
    tags = ansible_vars["gitlab_runners"][0]["tag_list"]
    http = urllib3.PoolManager()
    gitlab_runner_url = ansible_vars["gitlab_runners"][0]["gitlab_url"]
    url = f"{gitlab_runner_url}/api/v4/runners/{gitlab_runner_id}"

    request = http.request(
        "GET",
        url,
        headers={
            "PRIVATE-TOKEN": f"{os.environ['GITLAB_RUNNER_API_TOKEN']}"},
    )

    configured_tags = json.loads(request.data.decode("utf-8"))["tag_list"]

    assert sorted(tags) == sorted(configured_tags)


def test_runner_registered(host):
    runner_status = host.run("gitlab-runner verify")

    assert runner_status.rc == 0
