import os
import pytest
import yaml


@pytest.fixture
def ansible_vars():
    pth = os.path.realpath(
        os.path.join(__file__, "..", "..", "side_effect.yml")
    )
    with open(pth) as ymlfile:
        ansible_vars = yaml.safe_load(ymlfile)[0]["tasks"][0]["vars"]

    return ansible_vars
