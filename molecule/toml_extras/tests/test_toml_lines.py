import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_runner_registered(host):
    config_toml = host.file('/etc/gitlab-runner/config.toml')
    assert ('extra_hosts = ["testolagitlab.com:172.65.251.78"]'
            in config_toml.content_string)


def test_runner_docker_env_config(host):
    config_toml = host.file('/etc/gitlab-runner/config.toml')
    assert ('["DOCKER_AUTH_CONFIG={\\"auths\\":{\\"https://index.docker.io/v1/\\":{'
            in config_toml.content_string)
