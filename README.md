# Ansible Role Gitlab Runner

Deploys a Gitlab Runner

## Requirements

This role requires a number of additional roles to deploy. Please refer to the `requirements.yml`

`ansible-galaxy install -r requirements.yml`

## Role Variables

| Variable                          | Required | Default Value                | Explanation                                      |
|:----------------------------------|:---------|:-----------------------------|:-------------------------------------------------|
| gitlab_runner_url                 | True     | None                         | The URL of the Gitlab instance                   |
| gitlab_runner_api_token           | True     | None                         | A Gitlab API token. Must have API write access   |
| gitlab_runner_registration_token  | True     | None                         | Registration token for the runner                |
| gitlab_runner_name                | True     | None                         | The name of the Gitlab runner                    |
| gitlab_runners                    | True     | None                         | List of gitlab_runner config objects below       |
| gitlab_runner_repositories        | False    | See in defaults/main.yml  | List of repositories to use for installing the Gitlab runner    |
| gitlab_runner_concurrent_jobs     | False    | 4                         | Number of concurrent jobs to run across all registered runners  |
| gitlab_runner_check_interval      | False    | 0                         | Seconds between job checks                    |
| gitlab_runner_limit               | False    | 4                         | Number of concurrent jobs to run              |
| gitlab_runner_run_untagged        | False    | False                     | Whether the runner can run untagged jobs      |
| gitlab_runner_docker_image        | False    | ubuntu:20.04              | Default image to use for the Docker executor  |
| gitlab_runner_docker_pull_policy  | False    | if-not-present            | The docker image pull policy for the docker executor        |
| gitlab_runner_docker_privileged      | False | false                     | Allow privileged docker containers to be run  |
| gitlab_runner_docker_security_opt    | False | []                        | List of security opts to pass to Docker       |
| gitlab_runner_seccomp_default_action | False | SCMP_ACT_ERRNO            | The seccomp policy default action for restricting           |
| gitlab_runner_seccomp_extra_syscalls | False | None                      | Additional syscalls to allow in the seccomp policy          |
| gitlab_runner_extra_hosts            | False | Undefined                 | /etc/hosts entries to be added to each container --add-host |

### gitlab_runners objects
| gitlab_runners[].Variable | Required | Default Value | Explanation                                  |
|:--------------------------|:---------|:--------------|:---------------------------------------------|
| executor                  | True     | None          | Executor type choose (docker,docker+machine,shell)                |
| name                      | True     | None          | Unique name for each runner string (docker,docker+machine,shell)  |
| gitlab_url                | True     | None          | String url to gitlab instance (docker,docker+machine,shell)       |
| api_token                 | True     | None          | String your api_token for registering and modifying runners to your instance (docker,docker+machine,shell) |
| description               | True     | None          | String url to gitlab instance (docker,docker+machine,shell)       |
| registration_token        | True     | None          | String token to register runner to org/project (docker,docker+machine,shell)    |
| tag_list                  | False    | None          | List of string tags for runner instance                                         |
| limit                     | False    | gitlab_runner_limit        | String numeric limit for each runner (docker,docker+machine,shell) |
| docker_image              | False    | gitlab_runner_docker_image | String default docker image for executors (docker,docker+machine) |
| docker_security_opt       | False    | None                       | A list of security opts to pass to docker              |
| extra_hosts               | False    | None                       | String default docker image for executors (docker,docker+machine) |
| docker_pull_policy        | False    | gitlab_runner_docker_pull_policy        | String default docker image for executors (docker,docker+machine) |
| machine_idle_count        | False    | gitlab_runner_machine_idle_count        | Docker machine idle count (docker+machine) |
| machine_idle_count_min    | False    | gitlab_runner_machine_idle_count_min    | Docker machine idle count min (docker+machine) |
| machine_idle_scale_factor | False    | gitlab_runner_machine_idle_scale_factor | Docker machine idle scale factor (docker+machine) |
| machine_driver            | False    | gitlab_runner_machine_driver            | Docker machine driver (docker+machine) |
| machine_name              | False    | gitlab_runner_machine_name              | Docker machine name pattern (docker+machine) |
| max_builds                | False    | gitlab_runner_max_builds                | Docker machine max builds (docker+machine) |
| machine_options           | False    | gitlab_runner_machine_options           | List of Docker machine machine_options (docker+machine) |
| trusted_certificate_path  | False    | None                                    | Trusted certificates (docker,docker+machine) |
| autoscale                 | False    | None                                    | Trusted certificates (docker,docker+machine) |

### gitlab_runner.autoscale objects (docker+machine)
| gitlab_runners[].autoscale.Variable  | Required | Default Value | Explanation                                     |
|:-------------------------------------|:---------|:--------------|:------------------------------------------------|
| periods                              | True     | None          | Policy period (see docker+machine docs)         |
| idle_count                           | False    | '0'           | Policy idle_count (see docker+machine docs)     |
| idle_count_min                       | False    | '0'           | Policy idle_count_min (see docker+machine docs) |
| idle_scale_factor                    | False    | '0.0'         | Policy idle_count_min (see docker+machine docs) |
| idle_time                            | False    | '600'         | Policy idle_time (see docker+machine docs)      |
| tz                                   | False    | 'tz'          | Policy tz (see docker+machine docs)             |

## Example Playbook

See examples in the `molecule/<scenario>` directories.

## License

MIT

## Author Information

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
