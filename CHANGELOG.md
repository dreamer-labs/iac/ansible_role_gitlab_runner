## [1.5.1](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.5.0...v1.5.1) (2022-06-01)


### Bug Fixes

* trust ubuntu cert store manually ([eeb65e7](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/eeb65e7))

# [1.5.0](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.4.0...v1.5.0) (2022-05-25)


### Features

* add a starting flush handlers ([d5ebcdb](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/d5ebcdb))
* remove handlers because they are bad ([751679b](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/751679b))

# [1.4.0](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.3.0...v1.4.0) (2022-05-20)


### Features

* apt repos now use signed-by ([c7c97ab](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/c7c97ab))
* ignore 503 must use handlers ([0f6e3da](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/0f6e3da))
* override default umask when installing repo ([17a8051](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/17a8051))

# [1.3.0](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.2.0...v1.3.0) (2022-04-06)


### Bug Fixes

* fixed cleanup runners ([efb32e1](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/efb32e1))


### Features

* added extra toml option gitlab_runner_extra_hosts ([d2f86dc](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/d2f86dc))

# [1.2.0](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.1.0...v1.2.0) (2022-04-05)


### Features

* add ubuntu support ([2144680](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/2144680))

# [1.1.0](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/compare/v1.0.0...v1.1.0) (2022-01-27)


### Bug Fixes

* allow tag_list length to be zero ([c9f8238](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/c9f8238))


### Features

* add support for additional syscalls ([9c58015](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/9c58015))

# 1.0.0 (2022-01-26)


### Bug Fixes

* enable docker service ([943ad99](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/943ad99))
* Fix docker login bug ([4550539](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/4550539))
* fix docker service ([0ae8cde](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/0ae8cde))
* fix intermittent failure in test ([59d75a8](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/59d75a8))
* gpg fix ([e392a86](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/e392a86))
* Upgrade to ansible 4.7 (ansible-core 2.11.6) ([9c2ebea](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/9c2ebea))


### Features

* add docker authentication ([f8b52ef](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/f8b52ef))
* Add docker login cron ([9fde8aa](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/9fde8aa))
* add listen address feature for metrics ([7998466](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/7998466))
* Add semantic versioning to the repo ([af31e59](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/af31e59))
* add tag support ([06229d2](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/06229d2))
* add trusted cert configuration ([49421d6](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/49421d6))
* Added rootless functionality ([efd8b4f](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/efd8b4f))
* initial commit ([813dbfa](https://gitlab.com/dreamer-labs/iac/ansible_role_gitlab_runner/commit/813dbfa))
